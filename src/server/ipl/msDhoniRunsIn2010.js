/**
 * How may runs scored by MS Dhoni in the year of 2010.
 * 
 * @param {Array<Object>} deliveriesArray Scorecard details of all season
 * @param {Array<Object>} matchesArray Match details of all season
 * @returns {Object}
 */

function msDhoniRunsIn2010(deliveriesArray, matchesArray) {

    let totalRunsByMSD = 0;
    //filter the matchesArray to 2010'th matches only
    matchesArray = matchesArray.filter(element => {
        return (element.season == 2010);
    });

    //filter the deliveriesArray to 2010'th scorecard only
    deliveriesArray = deliveriesArray.filter(element => {
        return (element.match_id >= Number(matchesArray[0].id) && element.match_id <= Number(matchesArray[matchesArray.length - 1].id));
    });

    //filter the deliveriesArray to MS Dhoni batsman scorecard only
    deliveriesArray = deliveriesArray.filter(element => {
        return (element.batsman == 'MS Dhoni');
    });

    //reduce the total runs by MSD
    totalRunsByMSD = deliveriesArray.reduce((accumulator, currentValue) => {
        return (accumulator + Number(currentValue.batsman_runs));
    }, totalRunsByMSD);

    //change to object
    totalRunsByMSD = {
        'Total': totalRunsByMSD
    };
    return totalRunsByMSD;
}

module.exports = msDhoniRunsIn2010;