/**
 * Number of winnings per team per season.
 * 
 * @param {Array<Object>} matchesArray Match details of all season
 * @returns {Object} 
 */
function matchesWonPerTeam(matchesArray) {
    let matchesWonPerTeam;

    // Initialize the season as key and {} as value
    matchesWonPerTeam = matchesArray.map(function (element) {
        this[element.season] = {};
        return this;
    }, {});
    matchesWonPerTeam = matchesWonPerTeam[0];

    // Initialize the team name as key and 0 as value for each season object
    matchesWonPerTeam = matchesArray.map(function (element) {
        if (element.team1 != '' && element.team2 != '') {
            this[element.season][element.team1] = 0;
            this[element.season][element.team2] = 0;
        }
        return this;
    }, matchesWonPerTeam);
    matchesWonPerTeam = matchesWonPerTeam[0];

    // Using reduce we add the count for particular team for particular season
    matchesWonPerTeam = matchesArray.reduce((accumulator, currentValue) => {
        if (currentValue.winner != '') {
            accumulator[currentValue.season][currentValue.winner] += 1;
        }
        return accumulator;
    }, matchesWonPerTeam);

    return matchesWonPerTeam;
}

module.exports = matchesWonPerTeam;