/**
 * This index.js used to export all the sub-functions to the main index.js file
 */

const writeIntoJsonOutput = require("./writeIntoJsonOutput.js");
const matchesPlayedPerYear = require("./matchesPlayedPerYear.js");
const matchesWonPerTeam = require("./matchesWonPerTeam.js")
const extraRunsPerTeamIn2016 = require("./extraRunsPerTeamIn2016.js");
const topTenEconomicalBowls = require("./topTenEconomicalBowls.js");
const msDhoniRunsIn2010 = require("./msDhoniRunsIn2010.js");

module.exports = {
    writeIntoJsonOutput,
    matchesPlayedPerYear,
    matchesWonPerTeam,
    extraRunsPerTeamIn2016,
    topTenEconomicalBowls,
    msDhoniRunsIn2010
};