/**
 * Extra runs conceded per team in the season of 2016.
 * 
 * @param {Array<Object>} deliveriesArray Scorecard details of all season
 * @param {Array<Object>} matchesArray Match details of all season
 * @returns {Object}
 */

function extraRunsPerTeamIn2016(deliveriesArray, matchesArray) {
    let extraRunsPerTeamIn2016;

    //filters matchesArray to 2016'th matches only
    matchesArray = matchesArray.filter(element => {
        return element.season == 2016;
    });

    //filters deliveriesArray to 2016'th scorecard only
    deliveriesArray = deliveriesArray.filter(element => {
        return ((element.match_id >= Number(matchesArray[0].id)) && (element.match_id <= Number(matchesArray[matchesArray.length - 1].id)))
    });

    //Creates all the teams in object and initialize it to 0.
    extraRunsPerTeamIn2016 = deliveriesArray.map(function (element) {
        if (element.batting_team != '') {
            this[element.batting_team] = 0;
        }
        return this;
    }, {});
    extraRunsPerTeamIn2016 = extraRunsPerTeamIn2016[0];


    //Extra runs added to the particular bowling team in object
    extraRunsPerTeamIn2016 = deliveriesArray.reduce((accumulator, currentValue) => {
        accumulator[currentValue.bowling_team] += parseInt(currentValue.extra_runs);
        return accumulator;
    }, extraRunsPerTeamIn2016);

    return extraRunsPerTeamIn2016;
}

module.exports = extraRunsPerTeamIn2016;