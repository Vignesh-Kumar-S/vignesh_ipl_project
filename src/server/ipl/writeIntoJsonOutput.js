/**
 * Write output into the particular JSON files in output folder
 * 
 * @param {Object} output Output
 * @param {String} outputPath where to store this output
 * @param {Object} fs how to store that output
 */
function writeIntoJsonOutput(output, outputPath, fs) {
    fs.writeFileSync(outputPath, JSON.stringify(output), "utf-8", (err) => {
        if (err) {
            console.log(err);
        }
    });
}

module.exports = writeIntoJsonOutput;