/**
 * Total number of matches played per year in IPL.
 * 
 * @param {Array<Object>} matchesArray Match details of all season
 * @returns {Object} 
 */

function matchesPlayedPerYear(matchesArray) {

   let matchesPlayedPerYear;

   // Initialize the season as key and 0 as value
   matchesPlayedPerYear = matchesArray.map(function (element) {
      this[element.season] = 0;
      return this;
   }, {});
   matchesPlayedPerYear = matchesPlayedPerYear[0];

   // Using reduce we add the count for particular season
   matchesPlayedPerYear = matchesArray.reduce((accumulator, currentValue) => {
      accumulator[currentValue.season] += 1;
      return accumulator;
   }, matchesPlayedPerYear);

   return matchesPlayedPerYear;
}

module.exports = matchesPlayedPerYear;