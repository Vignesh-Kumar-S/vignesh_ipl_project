/**
 * Top 10 economical bowlers in the year 2015
 * 
 * @param {Array<Object>} deliveriesArray Scorecard details of all season
 * @param {Array<Object>} matchesArray Match details of all season
 * @param {Number} n Top n Economical bowler
 * @returns {Object}
 */
function topTenEconomicalBowls(deliveriesArray, matchesArray, n) {
    let topTenBowler;

    // filters matchesArray to 2015'th season matches only
    matchesArray = matchesArray.filter(element => {
        return element.season == '2015';
    });

    // filters deliveriesArray to 2015'th match scorecard only
    deliveriesArray = deliveriesArray.filter(element => {
        return ((element.match_id >= Number(matchesArray[0].id)) && (element.match_id <= Number(matchesArray[matchesArray.length - 1].id)));
    });

    // Initialize all the bowlers of their 'Runs' 'Balls' 'Economy' with 0
    topTenBowler = deliveriesArray.map(function (element) {

        this[element.bowler] = {};
        this[element.bowler]['Balls'] = 0;
        this[element.bowler]['Runs'] = 0;
        this[element.bowler]['Economy'] = 0;
        return this;
    }, {});
    topTenBowler = topTenBowler[0];

    // Add all the runs of particular bowler
    topTenBowler = deliveriesArray.reduce((accumulator, currentValue) => {
        if (currentValue.is_super_over != 0 || currentValue.bye_runs != 0 || currentValue.legbye_runs != 0 || currentValue.penalty_runs != 0) { }
        else {
            accumulator[currentValue.bowler]['Runs'] += Number(currentValue.total_runs);
        }
        return accumulator;
    }, topTenBowler);

    // Add all the balls of particular bowler
    topTenBowler = deliveriesArray.reduce((accumulator, currentValue) => {
        if (currentValue.is_super_over == 0 && currentValue.wide_runs == 0 && currentValue.noball_runs == 0) {
            accumulator[currentValue.bowler]['Balls'] += 1;
        }
        return accumulator;
    }, topTenBowler);

    // Calculate Economy
    topTenBowler = Object.entries(topTenBowler).map(function (element) {
        this[element[0]]['Economy'] = parseFloat(parseFloat(this[element[0]]['Runs']) / parseFloat(this[element[0]]['Balls'])) * 6;
        return this;
    }, topTenBowler);
    topTenBowler = topTenBowler[0];

    //stores only economy to the Object
    topTenBowler = (Object.entries(topTenBowler).map(bowler => {
        bowler[1] = Number(bowler[1].Economy);
        return bowler;
    }));

    //Sort based on economy
    topTenBowler = topTenBowler.sort((a, b) => a[1] - b[1]);
    let tenthRate = topTenBowler[n - 1][1];

    //filters to less than or equal to the n'th rate
    topTenBowler = topTenBowler.filter(element => {
        return element[1] <= tenthRate;
    })

    //Array to Object
    topTenBowler = Object.fromEntries(topTenBowler);

    return topTenBowler;
}
module.exports = topTenEconomicalBowls;