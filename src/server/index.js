/**
 * This is the starting point of IPL Data Project.
 */

/**
 * Convert CSV to JSON
 * @type {function}
 */
const csvtojson = require('csvtojson');

/**
 * Work with the files in our PC
 * @type {object}
 */
const fs = require('fs');

/**
 * Work with the paths
 * @type {object}
 */
const path = require('path');

/**
 * Contains all the functions.
 * @type {object}
 */
const ipl = require("./ipl/index.js");

/**
 * Details about all the IPL matches.
 * @type {string}
 */
const csvMatchesPath = './../data/matches.csv';

/**
 * Scorecard of all the IPL matches
 * @type {string}
 */
const csvDeliveriesPath = './../data/deliveries.csv';

/**
 * Store the output of called function.
 * @type {object}
 */
let output = {};

/**
 * Store the output path to write into json file
 * @type {string}
 */
let outputPath;

csvtojson()
    .fromFile(csvMatchesPath)
    .then((matchesArray) => {

        output = ipl.matchesPlayedPerYear(matchesArray);
        outputPath = path.dirname('./../public/output/matchesPerYear.json');
        outputPath = path.join(outputPath, path.basename('./../public/output/matchesPerYear.json'));
        ipl.writeIntoJsonOutput(output, outputPath, fs);

        output = ipl.matchesWonPerTeam(matchesArray);
        outputPath = path.dirname('./../public/output/matchesWonPerTeam.json');
        outputPath = path.join(outputPath, path.basename('./../public/output/matchesWonPerTeam.json'));
        ipl.writeIntoJsonOutput(output, outputPath, fs);

        csvtojson()
            .fromFile(csvDeliveriesPath)
            .then((deliveriesArray) => {

                output = ipl.extraRunsPerTeamIn2016(deliveriesArray, matchesArray);
                outputPath = path.dirname('./../public/output/extraRunsConcededPerTeamIn2016.json');
                outputPath = path.join(outputPath, path.basename('./../public/output/extraRunsConcededPerTeamIn2016.json'));
                ipl.writeIntoJsonOutput(output, outputPath, fs);

                output = ipl.topTenEconomicalBowls(deliveriesArray, matchesArray, 10);
                outputPath = path.dirname('./../public/output/topTenEconomicalBowlersIn2015.json');
                outputPath = path.join(outputPath, path.basename('./../public/output/topTenEconomicalBowlersIn2015.json'));
                ipl.writeIntoJsonOutput(output, outputPath, fs);

                output = ipl.msDhoniRunsIn2010(deliveriesArray,matchesArray);
                outputPath = path.dirname('./../public/output/msDhoniRunsIn2010.json');
                outputPath = path.join(outputPath, path.basename('./../public/output/msDhoniRunsIn2010.json'));
                ipl.writeIntoJsonOutput(output,outputPath,fs);
            });
    });

